import numpy as np

def newton(x, y):
    divs = []

    def division(x, y):
        if len(y) > 2:
            return (division(x[:-1], y[:-1]) - division(x[1:], y[1:])) / (x[0] - x[-1])

        else:
            return (y[0] - y[1]) / (x[0] - x[1])

    for i in range(2, len(x) + 1):
        divs = np.append(divs, division(x[:i], y[:i]))

    def poly(z):
        def gorner(zz, xx, ddivs):
            if len(ddivs) == 0:
                return 0
            else:
                return (zz - xx[0]) * (ddivs[0] + gorner(zz, xx[1:], ddivs[1:]))

        return y[0] + gorner(z, x, divs)

    return poly

def lagrange(x, y):
    def lapoly(z, k):
        return np.prod([(z - x[i]) / (x[k] - x[i]) for i in np.arange(len(x)) if i != k])

    return lambda z: np.sum([y[i] * lapoly(z, i) for i in np.arange(len(x))])

def msq(x, y, polylen=5):
    xsquares = np.array([np.sum(x ** i) for i in np.arange(2 * polylen - 1)])

    matrix = np.empty([polylen, polylen])
    for i in np.arange(polylen):
        for j in np.arange(polylen):
            matrix[i][j] = xsquares[2 * (polylen - 1) - (i + j)]

    ans = np.array([np.sum(y * (x ** i)) for i in np.arange(polylen)[::-1]])

    def maketriangle(matrix, ans):
        for i in np.arange(len(ans)):
            for j in np.arange(i + 1, len(ans)):
                factor = matrix[i, i] / matrix[j, i]
                matrix[j] = (factor * matrix[j]) - matrix[i]
                ans[j] = (factor * ans[j]) - ans[i]

        return matrix, ans

    def solvetriangle(matrix, ans):
        xx = np.zeros(len(ans), dtype=float)

        for i in np.arange(len(ans))[::-1]:
            xx[i] = 1 / matrix[i][i] * (ans[i] - np.sum(
                [matrix[i][len(ans) - 1 - j] * xx[len(ans) - 1 - j] for j in np.arange(len(ans) - 1 - i)]))

        return xx

    A, b = maketriangle(matrix, ans)
    factors = solvetriangle(A, b)[::-1]

    return lambda x: np.sum([factors[i] * (x ** i) for i in np.arange(len(factors))])

def spline(x, y):

    n = len(x)

    c = np.zeros(n, dtype=float)
    b = np.zeros(n, dtype=float)
    d = np.zeros(n, dtype=float)

    alpha = np.zeros(n - 1, dtype=float)

    beta = np.zeros(n - 1, dtype=float)

    for i in np.arange(1, n - 1):
        hi = x[i] - x[i - 1]
        hi_1 = x[i + 1] - x[i]

        A = hi
        C = 2 * (hi + hi_1)
        B = hi_1
        F = 6 * ((y[i + 1] - y[i]) / hi_1 - (y[i] - y[i - 1]) / hi)

        z = (A * alpha[i - 1] + C)

        alpha[i] = -B / z
        beta[i] = (F - A * beta[i - 1]) / z

    # TODO control it!
    c[-1] = (F - A * beta[-1]) / (C + A * alpha[-1])

    for i in np.arange(1, n - 1)[::-1]:
        c[i] = alpha[i] * c[i + 1] + beta[i]

    for i in np.arange(1, n)[::-1]:
        hi = x[i] - x[i - 1]
        d[i] = (c[i] - c[i - 1]) / hi
        b[i] = hi * (2 * c[i] + c[i - 1]) / 6 + (y[i] - y[i - 1]) / hi

    splines = np.array([])
    for i in np.arange(len(d)):
        splines = np.append(splines, lambda z, ii=i: y[ii] + b[ii] * (z - x[ii]) + c[ii] / 2 * ((z - x[ii]) ** 2) + d[ii] / 6 * ((z - x[ii]) ** 3))

    def plot(z):
        if z <= np.min(x):
            return splines[1](z)
        elif z >= np.max(x):
            return splines[-1](z)

        for i in np.arange(1, len(splines)):
            # pdb.set_trace()
            if z >= x[i - 1] and z < x[i]:
                return splines[i](z)

        return 0

    return plot