from PyQt5.QtWidgets import (QWidget, QTabWidget, QVBoxLayout, QHBoxLayout, QGridLayout,
                             QCheckBox, QPushButton, QLineEdit, QLabel,
                             QSlider)
from PyQt5.QtChart import QChart, QChartView, QLineSeries
from PyQt5.QtCore import Qt

from .pde_alg import pde, U_t0, U_xa


class Lab3(QWidget):
    def __init__(self):
        super(Lab3, self).__init__()

        self.U = None
        self.X = None
        self.T = None
        self.parameters = {}
        self.by_id = lambda id: next(float(i['line'].text()) for i in self.parameterList if i['id'] == id)


        self.initUI()

        for p in self.parameterList:
            p['line'].setText(str(p['default']))

        self.uxPlot.setName('Распределение теплоты')
        self.uxChart.addSeries(self.uxPlot)
        self.uxChart.createDefaultAxes()
        self.uxChartView.setChart(self.uxChart)

        self.utCenterPointPlot.setName('Зависимость температуры от времени в центре')
        self.utBoundaryPointPlot.setName('Зависимость температуры от времени на границе')

        self.utChart.addSeries(self.utCenterPointPlot)
        self.utChart.addSeries(self.utBoundaryPointPlot)

        self.utChart.createDefaultAxes()
        self.utChartView.setChart(self.utChart)

        self.calculateButton.clicked.connect(self.recalculate)
        self.resetButton.clicked.connect(self.resetPlot)

        self.activePlotSlider.valueChanged.connect(self.resetUxPlot)

    def initUI(self):
        mainLayout = QHBoxLayout()

        plotTabWidget = QTabWidget()
        plotTabWidget.addTab(self.uxChartView, 'Координатная зависимость')
        plotTabWidget.addTab(self.utChartView, 'Временная зависимость')


        chartLayout = QVBoxLayout()
        chartLayout.addWidget(plotTabWidget)
        chartLayout.addWidget(self.activePlotSlider)

        buttonLayout = QHBoxLayout()
        buttonLayout.addWidget(self.calculateButton)
        buttonLayout.addWidget(self.resetButton)

        chartLayout.addLayout(buttonLayout)

        parametersLayout = QGridLayout()
        for (line, index) in zip(self.parameterList, range(len(self.parameterList))):
            parametersLayout.addWidget(QLabel(line['name']), index, 0)
            parametersLayout.addWidget(line['line'],index, 1)


        mainLayout.addLayout(chartLayout)
        mainLayout.addLayout(parametersLayout)

        mainLayout.setStretchFactor(chartLayout, 4)
        mainLayout.setStretchFactor(parametersLayout, 1)

        self.setLayout(mainLayout)

    def recalculate(self):
        self.calculatePDE()
        self.resetPlot(0)

    def resetUxPlot(self, time_index = 0):
        self.uxPlot.clear()

        for x, u in zip(self.X, self.U[time_index, :]):
            self.uxPlot.append(x, u)

        self.uxChart.axisY().setRange(0, self.by_id('y_max'))
        self.uxChartView.repaint()

    def resetPlot(self, time_index = 0):
        self.uxPlot.clear()
        self.utCenterPointPlot.clear()
        self.utBoundaryPointPlot.clear()

        middle_index = int(len(self.X)/2)
        u1 = self.U[:, middle_index][::10]
        u2 = self.U[:, -1][::10]
        t = self.T[::10]
        for t, uc, ub in zip(t, u1, u2):
            self.utCenterPointPlot.append(t, uc)
            self.utBoundaryPointPlot.append(t, ub)

        for x, u in zip(self.X, self.U[time_index, :]):
            self.uxPlot.append(x, u)

        self.uxChart.axisY().setRange(0, self.by_id('y_max'))
        self.utChart.axisY().setRange(0, self.by_id('y_max'))
        self.utChart.axisX().setRange(0, self.by_id('T2'))

        self.uxChartView.repaint()
        self.utChartView.repaint()

    def calculatePDE(self):
        settings = {}
        settings['xh'] = self.by_id('xh')
        settings['th'] = self.by_id('th')
        settings['T2'] = self.by_id('T2')
        settings['k'] = self.by_id('k')
        settings['U_t0'] = U_t0(wave_num=self.by_id('wave_num'),
                                amplitude=self.by_id('amplitude'))
        settings['U_xa'] = U_xa(self.by_id('k0'),
                                self.by_id('U0'),
                                self.by_id('alpha'))

        self.X, self.T, self.U = pde(settings)
        self.activePlotSlider.setRange(0, len(self.T))

    activePlotSlider = QSlider(Qt.Horizontal)
    calculateButton = QPushButton('Пересчитать')
    resetButton = QPushButton('Перестроить')

    uxChart = QChart()
    uxChartView = QChartView()
    uxPlot = QLineSeries()

    utChart = QChart()
    utChartView = QChartView()
    utCenterPointPlot = QLineSeries()
    utBoundaryPointPlot = QLineSeries()


    parameterList = [
        {
            'id': 'xh',
            'name': 'Координатный шаг',
            'default': 0.05,
            'line': QLineEdit()
        },
        {
            'id': 'th',
            'name': 'Временной шаг',
            'default': 0.0005,
            'line': QLineEdit()
        },
        {
            'id': 'k',
            'name': 'Коэффициент при Uxx',
            'default': 1,
            'line': QLineEdit()
        },
        {
            'id': 'T2',
            'name': 'Время',
            'default': 2,
            'line': QLineEdit()
        },
        {
            'id': 'k0',
            'name': 'Обобщённый коэффициент',
            'default': 1,
            'line': QLineEdit()
        },
        {
            'id': 'alpha',
            'name': 'Коэффициент усиления',
            'default': 200,
            'line': QLineEdit()
        },
        {
            'id': 'U0',
            'name': 'Напряжение на источнике',
            'default': 10,
            'line': QLineEdit()
        },
        {
            'id': 'wave_num',
            'name': 'Число волн',
            'default': 1,
            'line': QLineEdit()
        },
        {
            'id': 'amplitude',
            'name': 'Амплитуда волн',
            'default': 1,
            'line': QLineEdit()
        },
        {
            'id': 'y_max',
            'name': 'Верхняя граница графика',
            'default': 50,
            'line': QLineEdit()
        },
    ]

